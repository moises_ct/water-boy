using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTutorialTrigger : MonoBehaviour
{
    [Header("Sender Events")]
    [SerializeField] private VoidEventChannelSO _eventFinishTutorial;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _eventFinishTutorial.RaiseEvent();
        }
    }
}
