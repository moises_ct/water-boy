using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject _canvasEndTutorial;

    [Header("Listener Events")]
    [SerializeField] private VoidEventChannelSO _eventFinishTutorial;

    // Start is called before the first frame update
    void Start()
    {
        _eventFinishTutorial.OnEventRaised += ShowEndTutorial;
    }

    void OnDestroy()
    {
        _eventFinishTutorial.OnEventRaised -= ShowEndTutorial;
    }

    private void ShowEndTutorial()
    {
        _canvasEndTutorial.SetActive(true);
    }
}
