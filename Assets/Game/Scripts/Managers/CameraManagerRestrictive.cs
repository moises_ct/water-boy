using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagerRestrictive : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [Header("Values for camera")]
    [SerializeField] private float _offsetX;
    [SerializeField] private float _minY;
    [SerializeField] private float _maxY;

    private float _screenX;
    private float _screenY;

    private bool _followPlayer;

    private Transform _cameraTransform;

    private Camera _camera;

    public float ScreenX
    {
        get
        {
            return _screenX;
        }
    }

    public float ScreenY
    {
        get
        {
            return _screenY;
        }
    }

    public Transform CameraTransform
    {
        get
        {
            return _cameraTransform;
        }
    }

    public bool FollowPlayer
    {
        set
        {
            _followPlayer = value;
        }
    }

    void Awake()
    {
        _camera = GetComponent<Camera>();
        _followPlayer = true;
        CheckCamera();
    }

    // Use this for initialization
    void Start()
    {
        _cameraTransform = _camera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_followPlayer)
        {
            return;
        }
        float offsetX = _target.position.x - _cameraTransform.position.x;
        if (offsetX > this._offsetX)
        {
            float posX = _cameraTransform.position.x + (offsetX - this._offsetX);
            _cameraTransform.position = new Vector3(posX, _cameraTransform.position.y, _cameraTransform.position.z);
        }

        float offsetY = _target.position.y - _cameraTransform.position.y;
        if (offsetY > _maxY)
        {
            float posY = _cameraTransform.position.y + (offsetY - _maxY);
            _cameraTransform.position = new Vector3(_cameraTransform.position.x, posY, _cameraTransform.position.z);
        }
        if (offsetY < _minY)
        {
            float posY = _cameraTransform.position.y + (offsetY - _minY);
            _cameraTransform.position = new Vector3(_cameraTransform.position.x, posY, _cameraTransform.position.z);
        }
        CheckLeftSideCamera();
    }

    private void CheckLeftSideCamera()
    {
        if (_target.position.x < transform.position.x - _screenX)
        {
            _target.position = new Vector3(transform.position.x - _screenX, _target.position.y, _target.position.z);
        }
    }

    private void CheckCamera()
    {
        // When camera is ortographic.
        float height = 2 * _camera.orthographicSize;
        float width = height * _camera.aspect;
        _screenX = width / 2;
        _screenY = height / 2;
    }
}
