using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [Header("Values for camera")]
    [SerializeField] private float _minX;
    [SerializeField] private float _maxX;
    [SerializeField] private float _minY;
    [SerializeField] private float _maxY;

    private float _screenX;
    private float _screenY;

    private bool _followPlayer;

    private Transform _target;

    private Transform _cameraTransform;

    private Camera _camera;
    public Camera CurrentCamera => _camera;

    [Header("Values for Lerping Camera")]
    [SerializeField] private float _timeForLerp;
    [SerializeField] private AnimationCurve _curve;

    private bool _moveCamera;
    private Vector3 _initialPosition;
    private Vector3 _finalPosition;
    private float _timerLerp;

    public float ScreenX
    {
        get
        {
            return _screenX;
        }
    }

    public float ScreenY
    {
        get
        {
            return _screenY;
        }
    }

    public Transform CameraTransform
    {
        get
        {
            return _cameraTransform;
        }
    }

    public bool FollowPlayer
    {
        set
        {
            _followPlayer = value;
        }
    }

    void Awake()
    {
        _target = GameObject.FindGameObjectWithTag("Player").transform;
        _camera = GetComponent<Camera>();
        _followPlayer = true;
        CheckCamera();

        _moveCamera = false;
        _timerLerp = 0f;
    }

    // Use this for initialization
    void Start()
    {
        _cameraTransform = _camera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_followPlayer)
        {
            return;
        }
        CheckMove();
        float offsetX = _target.position.x - _cameraTransform.position.x;
        if (offsetX > _maxX)
        {
            float posX = _cameraTransform.position.x + (offsetX - _maxX);
            _cameraTransform.position = new Vector3(posX, _cameraTransform.position.y, _cameraTransform.position.z);
        }
        if (offsetX < _minX)
        {
            float posX = _cameraTransform.position.x + (offsetX - _minX);
            _cameraTransform.position = new Vector3(posX, _cameraTransform.position.y, _cameraTransform.position.z);
        }

        float offsetY = _target.position.y - _cameraTransform.position.y;
        if (offsetY > _maxY)
        {
            float posY = _cameraTransform.position.y + (offsetY - _maxY);
            _cameraTransform.position = new Vector3(_cameraTransform.position.x, posY, _cameraTransform.position.z);
        }
        if (offsetY < _minY)
        {
            float posY = _cameraTransform.position.y + (offsetY - _minY);
            _cameraTransform.position = new Vector3(_cameraTransform.position.x, posY, _cameraTransform.position.z);
        }
    }

    private void CheckMove()
    {
        if (_moveCamera)
        {
            if (_timerLerp > 0f)
            {
                _timerLerp -= Time.deltaTime;
                float percent = (_timeForLerp - _timerLerp) / _timeForLerp;
                Vector3 currentPos = Vector3.Lerp(_initialPosition, _finalPosition, _curve.Evaluate(percent));
                _cameraTransform.position = currentPos;
                if (_timerLerp <= 0f)
                {
                    _timerLerp = 0f;
                    _moveCamera = false;
                }
            }
        }

        #region TESTING
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            SetMoveUp();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SetMoveDown();
        }
        #endregion
    }

    public void SetMoveUp()
    {
        _moveCamera = true;
        _initialPosition = _cameraTransform.position;
        float offsetY = _target.position.y - _cameraTransform.position.y;
        _finalPosition = _cameraTransform.position + Vector3.up * (offsetY - _maxY);
        _timerLerp = _timeForLerp;
    }

    public void SetMoveDown()
    {
        _moveCamera = true;
        _initialPosition = _cameraTransform.position;
        float offsetY = _target.position.y - _cameraTransform.position.y;
        _finalPosition = _cameraTransform.position + Vector3.up * (offsetY - _minY);
        _timerLerp = _timeForLerp;
    }

    private void CheckLeftSideCamera()
    {
        if (_target.position.x < transform.position.x - _screenX)
        {
            _target.position = new Vector3(transform.position.x - _screenX, _target.position.y, _target.position.z);
        }
    }

    private void CheckCamera()
    {
        // When camera is ortographic.
        float height = 2 * _camera.orthographicSize;
        float width = height * _camera.aspect;
        _screenX = width / 2;
        _screenY = height / 2;
    }
}
