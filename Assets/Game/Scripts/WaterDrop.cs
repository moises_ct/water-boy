using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDrop : MonoBehaviour
{
    [Header("Listener Events")]
    [SerializeField] private IntEventChannelSO _eventWaterContainerFull;

    private Rigidbody2D _body;
    private float _timer;
    private int _containerId;

    public void SetValues(float timeToDisappear, Vector2 velocityFire)
    {
        _timer = timeToDisappear;
        _body.velocity = velocityFire;
    }

    void Awake()
    {
        _body = GetComponent<Rigidbody2D>();
        _containerId = 0;
    }

    void Start()
    {
        _eventWaterContainerFull.OnEventRaised += OnWaterContainerFull;
    }

    void OnDestroy()
    {
        _eventWaterContainerFull.OnEventRaised -= OnWaterContainerFull;
    }

    // Update is called once per frame
    void Update()
    {
        if (_containerId == 0 && _timer > 0)
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                _timer = 0;
                PoolWaterParticles.Instance.ReturnToPool(this);
                ResetValues();
                //Destroy(gameObject);
            }
        }
    }

    void OnWaterContainerFull(int idFromContainer)
    {
        if (_containerId == idFromContainer)
        {
            PoolWaterParticles.Instance.ReturnToPool(this);
            ResetValues();
        }   
    }

    void ResetValues()
    {
        _containerId = 0;
    }

    public void OnSetContainer(int idContainer)
    {
        _containerId = idContainer;
    }

    public void OnUnsetContainer()
    {
        _containerId = 0;
    }

    //void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("TriggerContainer"))
    //    {
    //        _containerId = true;
    //    }
    //}
    //void OnTriggerExit2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("TriggerContainer"))
    //    {
    //        _containerId = false;
    //    }
    //}


}
