using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textWateR;
    [SerializeField] private Image _imageWaterFill;
    [SerializeField] private Image _imageDashCooldownFill;

    [Header("Listener Events")]
    [SerializeField] private FloatEventChannelSO _eventUpdateWaterLevel;
    [SerializeField] private FloatEventChannelSO _eventUpdateDashCooldown;

    // Start is called before the first frame update
    void Start()
    {
        _eventUpdateWaterLevel.OnEventRaised += UpdateWaterLevel;
        _eventUpdateDashCooldown.OnEventRaised += UpdateDashCooldown;
    }

    void OnDestroy()
    {
        _eventUpdateWaterLevel.OnEventRaised -= UpdateWaterLevel;
        _eventUpdateDashCooldown.OnEventRaised -= UpdateDashCooldown;
    }

    private void UpdateWaterLevel(float waterLevel)
    {
        _imageWaterFill.fillAmount = waterLevel;
    }

    private void UpdateDashCooldown(float dashCooldown)
    {
        _imageDashCooldownFill.fillAmount = dashCooldown;
    }
}
