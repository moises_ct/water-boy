using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTutorialController : MonoBehaviour
{
    public void OnResetGame()
    {
        // Reload Scene.
        SceneManager.LoadScene(0);
    }
}
