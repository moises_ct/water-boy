using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BasicWaterContainer))]
public class BasicWaterContainerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        BasicWaterContainer waterContainer = (BasicWaterContainer)target;

        if (GUILayout.Button("Update Colliders"))
        {
            waterContainer.UpdateSizeOfContainer();
        }
    }
}
