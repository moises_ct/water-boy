using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleTarget : MonoBehaviour
{
    [SerializeField] private float _speedRotation;

    // Update is called once per frame
    void Update()
    {
        Vector3 currentEuler = transform.rotation.eulerAngles;
        Vector3 nextEuler = currentEuler + Vector3.forward * _speedRotation * Time.deltaTime;
        transform.rotation = Quaternion.Euler(nextEuler);
    }
}
