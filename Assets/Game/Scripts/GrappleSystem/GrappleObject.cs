using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleObject : MonoBehaviour
{
    [SerializeField] private GrappleTarget _grappleTarget;

    private bool _isGrappable;

    public bool IsGrappable => _isGrappable;

    [Header("Sender Events")]
    [SerializeField] private TransformEventChannelSO _eventAddGrappleForPlayer;
    [SerializeField] private TransformEventChannelSO _eventRemoveGrappleForPlayer;

    // Start is called before the first frame update
    void Start()
    {
        _isGrappable = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerTrigger"))
        {
            _grappleTarget.gameObject.SetActive(true);
            _isGrappable = true;
            _eventAddGrappleForPlayer.RaiseEvent(transform);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerTrigger"))
        {
            _grappleTarget.gameObject.SetActive(false);
            _isGrappable = false;
            _eventRemoveGrappleForPlayer.RaiseEvent(transform);
        }
    }
}
