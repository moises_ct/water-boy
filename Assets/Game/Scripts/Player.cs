using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum TOMMY_STATES
    {
        TOMMY,
        IN_TRANSFORMATION,
        WATER_BOY,
        WATER
    }

    [Header("Parameters")]
    [SerializeField] private float _speed;
    [SerializeField] private float _speedWaterMultiplier;
    [SerializeField] private float _jumpSpeed;
    [SerializeField] private float _dashTime;
    [SerializeField] private float _dashSpeed;
    [SerializeField] private float _dashHookSpeed;
    [SerializeField] private float _dashCooldown;

    [Header("Parameters for Water Ability")]
    [SerializeField] private float _waterLevel;
    [SerializeField] private float _maxWaterLevel;
    [Tooltip("Per Second")] [SerializeField] private float _waterProduction;
    [SerializeField] private float _waterUsePerParticle;

    [Header("Parameters for Water Particles")]
    [Tooltip("In milliseconds")]
    [SerializeField] private float _dashWaterTime;
    [Tooltip("In milliseconds")]
    [SerializeField] private float _launchWaterTime;
    [Tooltip("In milliseconds")]
    [SerializeField] private float _waterParticlesTime;
    [SerializeField] private float _speedLaunch;

    [Header("Ground Detection")]
    [SerializeField] private float _radius;
    [SerializeField] private LayerMask _mask;

    [Header("HandShoot")]
    [SerializeField] private Transform _handShoot;

    [Header("Sender Events")]
    [SerializeField] private FloatEventChannelSO _eventUpdateWaterLevel;
    [SerializeField] private FloatEventChannelSO _eventUpdateDashCooldown;

    [Header("Listener Events")]
    [SerializeField] private TransformEventChannelSO _eventAddGrappleForPlayer;
    [SerializeField] private TransformEventChannelSO _eventRemoveGrappleForPlayer;

    private TOMMY_STATES _state;

    private Camera _cameraGame;
    private Rigidbody2D _body;
    private Collider2D _collider;
    private Animator _animator;

    private List<ContactPoint2D> _contacts;

    private float _currentSpeedMultiplier;
    private bool _isInGround;

    private bool _isLaunchingWater;
    private float _timeParticleLaunch;

    private bool _isDashing;
    private float _timeDash;
    private float _timeParticleDash;
    private float _timeCooldownDash;

    private float _timeParticlesWater;

    [Header("Grapple")]
    [SerializeField] private bool _hooked;
    [SerializeField] private LayerMask _hookLayerMask;
    private LineRenderer _line;
    private DistanceJoint2D _rope;
    private List<Transform> _grappleReferences;
    private Transform _currentGrapple;

    void Awake()
    {
        _cameraGame = Camera.main;

        _body = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _animator = GetComponent<Animator>();

        _contacts = new List<ContactPoint2D>();
        _grappleReferences = new List<Transform>();
        _currentGrapple = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        _eventAddGrappleForPlayer.OnEventRaised += AddGrapple;
        _eventRemoveGrappleForPlayer.OnEventRaised += RemoveGrapple;

        _waterLevel = 0;

        _handShoot.gameObject.SetActive(false);

        _state = TOMMY_STATES.TOMMY;

        _currentSpeedMultiplier = 1;

        _isLaunchingWater = false;
        _timeParticleLaunch = 0;

        _isDashing = false;
        _timeDash = 0;
        _timeParticleDash = 0;
        _timeCooldownDash = _dashCooldown;

        _timeParticlesWater = _waterParticlesTime * 0.001f;

        _rope = GetComponent<DistanceJoint2D>();
        _line = GetComponent<LineRenderer>();

        _rope.enabled = false;
        _line.enabled = false;
    }

    private void RemoveGrapple(Transform grapple)
    {
        if (_grappleReferences.IndexOf(grapple) > -1)
        {
            _grappleReferences.Remove(grapple);
        }
        if (_grappleReferences.Count == 0)
        {
            if (_hooked)
            {
                UnsetGrappleHook(false);
            }
            _currentGrapple = null;
        }
    }

    private void AddGrapple(Transform grapple)
    {
        if (_grappleReferences.IndexOf(grapple) == -1)
        {
            _grappleReferences.Add(grapple);
        }
    }

    void OnDestroy()
    {
        _eventAddGrappleForPlayer.OnEventRaised -= AddGrapple;
        _eventRemoveGrappleForPlayer.OnEventRaised -= RemoveGrapple;
    }

    void FixedUpdate()
    {
        _isInGround = (Physics2D.OverlapCircle(transform.position, _radius, _mask) != null);
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDashing)
        {
            HandleGrappleHookMovement();
            if (_timeParticleDash > 0)
            {
                _timeParticleDash -= Time.deltaTime;
                if (_timeParticleDash <= 0)
                {
                    WaterDrop water = PoolWaterParticles.Instance.GetFromPool();
                    water.gameObject.SetActive(true);
                    water.transform.position = transform.position;
                    float speedWaterX = UnityEngine.Random.Range(-0.25f, 0.25f);
                    float speedWaterY = UnityEngine.Random.Range(-0.25f, 0.25f);
                    water.SetValues(1f, new Vector2(speedWaterX, speedWaterY));
                    _timeParticleDash = _dashWaterTime * 0.001f;
                    UseWaterLevel();
                }
            }
            if (_timeDash > 0)
            {
                _timeDash -= Time.deltaTime;
                if (_timeDash <= 0)
                {
                    _timeDash = 0;
                    _isDashing = false;
                }
            }
            return;
        }

        if (_timeCooldownDash < _dashCooldown)
        {
            _timeCooldownDash += Time.deltaTime;
            if (_timeCooldownDash >= _dashCooldown)
            {
                _timeCooldownDash = _dashCooldown;
            }
            float dashCooldownPercent = _timeCooldownDash / _dashCooldown;
            _eventUpdateDashCooldown.RaiseEvent(dashCooldownPercent);
        }

        if (_isLaunchingWater)
        {
            if (_timeParticleLaunch > 0)
            {
                _timeParticleLaunch -= Time.deltaTime;
                if (_timeParticleLaunch <= 0)
                {
                    WaterDrop water = PoolWaterParticles.Instance.GetFromPool();
                    water.gameObject.SetActive(true);
                    Vector2 waterLaunchPos = transform.position + Vector3.up * 0.1f;
                    water.transform.position = waterLaunchPos;
                    Vector2 positionMouse = _cameraGame.ScreenToWorldPoint(Input.mousePosition);
                    Vector2 vectorDirection = positionMouse - waterLaunchPos;
                    Vector2 vectorNormal = vectorDirection.normalized;
                    water.SetValues(5f, new Vector2(_speedLaunch * vectorNormal.x, _speedLaunch * vectorNormal.y));
                    float rotationZ = Mathf.Atan2(vectorDirection.y, vectorDirection.x);
                    rotationZ = (transform.eulerAngles.y > 0) ? (180f - rotationZ * Mathf.Rad2Deg) : (rotationZ * Mathf.Rad2Deg);
                    _handShoot.localRotation = Quaternion.Euler(_handShoot.localEulerAngles.x, _handShoot.localEulerAngles.y, rotationZ);
                    _timeParticleLaunch = _launchWaterTime * 0.001f;
                    UseWaterLevel();
                }
            }
        }

        CheckStates();

        float x = Input.GetAxisRaw("Horizontal");
        float speedX = x * _speed * _currentSpeedMultiplier;
        if (_state != TOMMY_STATES.IN_TRANSFORMATION)
        {
            if (speedX != 0)
            {
                transform.rotation = (speedX >= 0) ? (Quaternion.Euler(0, 0, 0)) : (Quaternion.Euler(0, 180, 0));
            }
            _body.velocity = new Vector2(speedX, _body.velocity.y);
            _animator.SetFloat("speedX", Mathf.Abs(speedX));
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (_isInGround)
                {
                    _body.velocity = new Vector2(_body.velocity.x, _jumpSpeed);
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                float velY = _body.velocity.y;
                if (velY > 0)
                {
                    _body.velocity = new Vector2(_body.velocity.x, velY * 0.5f);
                }
            }
        }
        if (_state == TOMMY_STATES.WATER_BOY)
        {
            // Implements Tommy's grapple hook
            HandleGrappleHookMovement();

            if (Input.GetKeyDown(KeyCode.LeftShift) && !_isDashing && _timeCooldownDash >= _dashCooldown)
            {
                float direction = (transform.eulerAngles.y > 0) ? -1 : 1;
                if (_hooked)
                {
                    _body.AddForce(new Vector2(direction * _dashHookSpeed, 0));
                }
                else
                {
                    _body.velocity = new Vector2(direction * _dashSpeed, 0);
                }
                _timeDash = _dashTime;
                _isDashing = true;
                _timeCooldownDash = 0;
                _timeParticleDash = _dashWaterTime * 0.001f;
            }
            if (Input.GetMouseButtonDown(0) && !_hooked)
            {
                _isLaunchingWater = true;
                _timeParticleLaunch = _launchWaterTime * 0.001f;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _isLaunchingWater = false;
            }
            _handShoot.gameObject.SetActive(_isLaunchingWater);
        }
        if (_state == TOMMY_STATES.WATER)
        {
            ShowWaterParticles(3);
        }
    }

    private void ShowWaterParticles(int waterDropCount)
    {
        if (_timeParticlesWater > 0)
        {
            _timeParticlesWater -= Time.deltaTime;
            if (_timeParticlesWater <= 0)
            {
                _timeParticlesWater = _waterParticlesTime * 0.001f;

                for (int i = 0; i < waterDropCount; i++)
                {
                    WaterDrop water = PoolWaterParticles.Instance.GetFromPool();
                    water.gameObject.SetActive(true);
                    water.transform.position = transform.position + Vector3.right * (i - 1) * 0.2f;
                    float speedWaterX = UnityEngine.Random.Range(-0.25f, 0.25f);
                    float speedWaterY = UnityEngine.Random.Range(-0.25f, 0.25f);
                    water.SetValues(0.75f, new Vector2(speedWaterX, speedWaterY));
                    UseWaterLevel();
                }
            }
        }
    }

    private void HandleGrappleHookMovement()
    {
        if (_grappleReferences.Count == 0)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!_hooked)
            {
                _currentGrapple = ReturnNearestGrapple();
                SetGrappleHook(_currentGrapple.position, true);
            }
            else
            {
                _currentGrapple = ReturnNearestGrapple();
                if (_currentGrapple != null)
                {
                    SetGrappleHook(_currentGrapple.position, true);
                }
                else
                {
                    UnsetGrappleHook(false);
                }
            }
        }
        if (_hooked)
        {
            ShowWaterParticles(2);
        }
        _line.SetPosition(1, transform.position);
    }

    private Transform ReturnNearestGrapple()
    {
        List<Transform> grappableList = GetGrappableList();
        if (grappableList.Count == 0)
        {
            return null;
        }
        Transform nearest;
        if (grappableList.Count == 1)
        {
            nearest = grappableList[0];
        }
        else
        {
            float nearestDistance = 100f;
            int indexNearest = -1;
            for (int i = 0; i < grappableList.Count; i++)
            {
                float distanceFromPlayer = (grappableList[i].position - transform.position).magnitude;
                if (distanceFromPlayer < nearestDistance)
                {
                    nearestDistance = distanceFromPlayer;
                    indexNearest = i;
                }
            }
            nearest = grappableList[indexNearest];
        }

        return nearest;
    }

    private List<Transform> GetGrappableList()
    {
        if (_currentGrapple == null)
        {
            return _grappleReferences;
        }
        // Ignore current grapple.
        List<Transform> grappableList = new List<Transform>();
        int indexCurrent = _grappleReferences.IndexOf(_currentGrapple);
        for (int i = 0; i < _grappleReferences.Count; i++)
        {
            if (i != indexCurrent)
            {
                grappableList.Add(_grappleReferences[i]);
            }
        }
        return grappableList;
    }

    public void SetGrappleHook(Vector3 positionGrapple, bool hooked)
    {
        _hooked = hooked;
        SetRope(positionGrapple);
        _animator.SetBool("IsHooked", _hooked);
        int direction = (transform.rotation.eulerAngles.y > 0f) ? (-1) : (1);
        // Values x 6 and y 12 are from the hook sprite, to adapt its anchor point
        // It's multiplied by 0.02f because of pixels per unit = 50 -> 1/50 = 0.02
        transform.position += new Vector3(direction * 6f * 0.02f, 12f * 0.02f, 0f);

        _isLaunchingWater = false;
        _handShoot.gameObject.SetActive(_isLaunchingWater);

        Debug.Log("Starting Grapple");
    }

    public void UnsetGrappleHook(bool hooked)
    {
        _hooked = hooked;
        ResetRope();
        Debug.Log("Ending Grapple Grapple");
        _animator.SetBool("IsHooked", _hooked);
        int direction = (transform.rotation.eulerAngles.y > 0f) ? (-1) : (1);
        transform.position -= new Vector3(direction * 6f * 0.02f, 12f * 0.02f, 0f);
    }

    void SetRope(Vector3 positionGrapple)
    {
        _rope.enabled = true;
        _rope.connectedAnchor = positionGrapple;

        _line.enabled = true;
        _line.SetPosition(0, positionGrapple);
    }

    void ResetRope()
    {
        _rope.enabled = false;
        _line.enabled = false;
    }

    private void UseWaterLevel()
    {
        _waterLevel -= _waterUsePerParticle;
        if (_waterLevel <= 0)
        {
            _waterLevel = 0;
            SetStateTommy();
        }
        UpdateWaterLevelToHUD();
    }

    private void UpdateWaterLevelToHUD()
    {
        float percentWaterLevel = _waterLevel / _maxWaterLevel;
        _eventUpdateWaterLevel.RaiseEvent(percentWaterLevel);
    }

    private void CheckStates()
    {
        switch (_state)
        {
            case TOMMY_STATES.TOMMY:
                if (_waterLevel < _maxWaterLevel)
                {
                    _waterLevel += Time.deltaTime * _waterProduction;
                    if (_waterLevel >= _maxWaterLevel)
                    {
                        _waterLevel = _maxWaterLevel;
                    }
                    UpdateWaterLevelToHUD();
                }
                if (Input.GetKeyDown(KeyCode.Z) && _waterLevel >= _maxWaterLevel)
                {
                    _animator.SetTrigger("SetWaterBoy");
                    _state = TOMMY_STATES.IN_TRANSFORMATION;
                    _body.velocity = Vector2.zero;
                }
                break;
            case TOMMY_STATES.IN_TRANSFORMATION:

                break;
            case TOMMY_STATES.WATER_BOY:
                if (IsInNarrowSpace())
                {
                    SetStateWater();
                }
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    SetStateTommy();
                }
                if (!_hooked && Input.GetKeyDown(KeyCode.X))
                {
                    SetStateWater();
                }
                break;
            case TOMMY_STATES.WATER:
                if (Input.GetKeyDown(KeyCode.X))
                {
                    _animator.SetTrigger("MoveAsBoy");
                    _state = TOMMY_STATES.WATER_BOY;
                    _currentSpeedMultiplier = 1;
                }
                break;
        }
    }

    private void SetStateWater()
    {
        _animator.SetTrigger("MoveAsWater");
        _state = TOMMY_STATES.WATER;

        _isLaunchingWater = false;
        _handShoot.gameObject.SetActive(_isLaunchingWater);

        _currentSpeedMultiplier = _speedWaterMultiplier;
    }

    private void SetStateTommy()
    {
        if (_hooked)
        {
            UnsetGrappleHook(false);
        }

        _animator.SetTrigger("UnsetWaterBoy");
        _state = TOMMY_STATES.IN_TRANSFORMATION;
        _body.velocity = Vector2.zero;

        _isLaunchingWater = false;
        _handShoot.gameObject.SetActive(_isLaunchingWater);

        _currentSpeedMultiplier = 1;
    }

    private bool IsInNarrowSpace()
    {
        int contacts = _collider.GetContacts(_contacts);
        bool contactGround = false;
        bool contactCeiling = false;

        if (contacts > 1)
        {
            for (int i = 0; i < contacts; i++)
            {
                if (_contacts[i].normal.y >= 0.9f)
                {
                    contactGround = true;
                }
                if (_contacts[i].normal.y <= -0.9f)
                {
                    contactCeiling = true;
                }
            }
        }

        return (contactGround && contactCeiling);
    }

    public void OnEndWaterBoyTransformation()
    {
        _state = TOMMY_STATES.WATER_BOY;
    }

    public void OnEndTommyTransformation()
    {
        _state = TOMMY_STATES.TOMMY;
    }
}
