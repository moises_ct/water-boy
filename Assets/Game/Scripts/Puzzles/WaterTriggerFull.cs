using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTriggerFull : MonoBehaviour
{
    [SerializeField] [Tooltip("In Seconds")] private float _timeToBeFull;
    [Header("Sender Events")]
    [SerializeField] private IntEventChannelSO _eventWaterContainerFull;

    private const int MIN_DROPS_TO_BE_FULL = 3;

    private BasicWaterContainer _basicWaterContainer;

    private int _idDoor;
    private float _fullTimer;
    private bool _isFull;
    private int _countWaterDrops;

    private void Awake()
    {
        Transform transformContainer = transform.parent;
        Transform transformDoorSystem = transformContainer.parent;
        _basicWaterContainer = transformDoorSystem.GetComponent<BasicWaterContainer>();
        //_basicWaterContainer = GetComponentInParent<BasicWaterContainer>();
        if (_basicWaterContainer != null)
        {
            _idDoor = _basicWaterContainer.Id;
        }
        else
        {
            _idDoor = 0;
            Debug.LogWarning("id is retrieved from DoorTriggerFull->Container->DoorSystem, check your prefab DoorSystem");
        }
        _fullTimer = 0;
        _isFull = false;
        _countWaterDrops = 0;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_isFull && collision.CompareTag("LiquidParticle"))
        {
            _fullTimer = _timeToBeFull;
            _countWaterDrops++;
            //Debug.Log("OnTriggerEnter2D WaterTriggerFull _countWaterDrops" + _countWaterDrops);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (!_isFull && collision.CompareTag("LiquidParticle"))
        {
            _countWaterDrops--;
            //Debug.Log("OnTriggerEnter2D WaterTriggerFull _countWaterDrops " + _countWaterDrops);
        }
    }

    void Update()
    {
        if (!_isFull && _countWaterDrops >= MIN_DROPS_TO_BE_FULL && _fullTimer > 0)
        {
            _fullTimer -= Time.deltaTime;
            if (_fullTimer <= 0)
            {
                _fullTimer = 0;
                _isFull = true;
                _countWaterDrops = 0;
                _basicWaterContainer.OnContainerFull();
                _eventWaterContainerFull.RaiseEvent(_idDoor);
            }
        }
    }
}
