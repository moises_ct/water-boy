using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDoor : MonoBehaviour
{
    [SerializeField] private int _id;

    [Header("Listener Events")]
    [SerializeField] private IntEventChannelSO _eventWaterContainerFull;

    void Start()
    {
        _eventWaterContainerFull.OnEventRaised += OnWaterContainerFull;
    }

    void OnDestroy()
    {
        _eventWaterContainerFull.OnEventRaised -= OnWaterContainerFull;
    }

    void OnWaterContainerFull(int idFromContainer)
    {
        if (_id == idFromContainer)
        {
            gameObject.SetActive(false);
        }
    }
}
