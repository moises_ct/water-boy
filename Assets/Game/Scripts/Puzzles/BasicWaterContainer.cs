using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicWaterContainer : MonoBehaviour
{
    [SerializeField] private int _id;
    [Header("Values to modify")]
    [SerializeField] private SpriteRenderer _spriteWaterContainer;
    [SerializeField] private BoxCollider2D _leftCollider;
    [SerializeField] private BoxCollider2D _bottomCollider;
    [SerializeField] private BoxCollider2D _rightCollider;
    [SerializeField] private Transform _triggerContainerTransform;
    [SerializeField] private BoxCollider2D _triggerFullCollider;

    private WaterTriggerContainer _triggerContainer;

    public int Id { get => _id; }

    void Awake()
    {
        _triggerContainer = GetComponentInChildren<WaterTriggerContainer>();
    }

    public void UpdateSizeOfContainer()
    {
        float currentWidth = _spriteWaterContainer.size.x;
        float currentHeight = _spriteWaterContainer.size.y;

        Debug.Log($"currentWidth {currentWidth}");
        Debug.Log($"currentHeight {currentHeight}");
        _leftCollider.offset = new Vector2(-currentWidth * 0.5f + 0.04f, 0f);
        _leftCollider.size = new Vector2(0.08f, currentHeight);

        _bottomCollider.offset = new Vector2(0, -currentHeight * 0.5f + 0.04f);
        _bottomCollider.size = new Vector2(currentWidth, 0.08f);

        _rightCollider.offset = new Vector2(currentWidth * 0.5f - 0.04f, 0f);
        _rightCollider.size = new Vector2(0.08f, currentHeight);

        float factorScaleSize = 25f / 16f;
        float containerSideSizeInScale = 0.25f;
        float newScaleX = factorScaleSize * currentWidth - containerSideSizeInScale;
        float newScaleY = factorScaleSize * currentHeight - containerSideSizeInScale;
        _triggerContainerTransform.localScale = new Vector3(newScaleX, newScaleY, 1f);

        _triggerFullCollider.offset = new Vector2(0f, currentHeight * 0.5f - 0.04f);
        _triggerFullCollider.size = new Vector2(currentWidth - 0.16f, 0.08f);
    }

    public void OnContainerFull()
    {
        _triggerContainer.OnContainerFull();
    }
}
