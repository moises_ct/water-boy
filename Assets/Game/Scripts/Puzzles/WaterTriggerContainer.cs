using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTriggerContainer : MonoBehaviour
{
    private int _idDoor;

    private Collider2D _collider;
    private SpriteRenderer _sprite;

    private void Awake()
    {
        Transform transformContainer = transform.parent;
        Transform transformDoorSystem = transformContainer.parent;
        BasicWaterContainer waterContainer = transformDoorSystem.GetComponent<BasicWaterContainer>();
        if (waterContainer != null)
        {
            _idDoor = waterContainer.Id;
        }
        else
        {
            _idDoor = 0;
            Debug.LogWarning("id is retrieved from DoorTriggerContainer->Container->DoorSystem, check your prefab DoorSystem");
        }

        _collider = GetComponent<Collider2D>();
        _sprite = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LiquidParticle"))
        {
            WaterDrop water = collision.GetComponent<WaterDrop>();
            water.OnSetContainer(_idDoor);
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("LiquidParticle"))
        {
            WaterDrop water = collision.GetComponent<WaterDrop>();
            water.OnUnsetContainer();
        }
    }

    public void OnContainerFull()
    {
        _collider.isTrigger = false;
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1f);
    }
}
