using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolWaterParticles : MonoBehaviour
{
    public static PoolWaterParticles Instance;

    private Stack<WaterDrop> _waterStack;

    [SerializeField] private int _initialSize;
    [SerializeField] private WaterDrop _prefabLiquid;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        _waterStack = new Stack<WaterDrop>();
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < _initialSize; i++)
        {
            _waterStack.Push(InstantiateAndDeactivate());
        }
    }

    public WaterDrop GetFromPool()
    {
        if ( _waterStack.Count > 0)
        {
            return _waterStack.Pop();
        }
        else
        {
            return InstantiateAndDeactivate();
        }
    }

    public void ReturnToPool(WaterDrop water)
    {
        water.gameObject.SetActive(false);
        _waterStack.Push(water);
    }

    WaterDrop InstantiateAndDeactivate()
    {
        WaterDrop water = Instantiate(_prefabLiquid, transform);
        water.gameObject.SetActive(false);
        return water;
    }
}
