using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleSurface : MonoBehaviour
{
    private SpriteRenderer surface;
    public Color selected, defaultColor;

    private void Start() {
        surface = GetComponent<SpriteRenderer>();
        
    }
    private void OnMouseOver() {
        surface.color = selected;
    }

    private void OnMouseExit() {
        surface.color = defaultColor;
    }
}
