using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZone : MonoBehaviour
{
    public enum CAMERA_MOVEMENT
    {
        MOVE_CAMERA_UP,
        MOVE_CAMERA_DOWN
    }

    [SerializeField] private CAMERA_MOVEMENT _moveCamera;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameObject[] cameraObjects = new GameObject[2];
            cameraObjects[0] = GameObject.FindGameObjectWithTag("MainCamera");
            cameraObjects[1] = GameObject.FindGameObjectWithTag("FluidCamera");
            for (int i = 0; i < cameraObjects.Length; i++)
            {
                CameraManager camera = cameraObjects[i].GetComponent<CameraManager>();
                if (camera != null)
                {
                    if (_moveCamera == CAMERA_MOVEMENT.MOVE_CAMERA_UP)
                    {
                        camera.SetMoveUp();
                    }
                    if (_moveCamera == CAMERA_MOVEMENT.MOVE_CAMERA_DOWN)
                    {
                        camera.SetMoveDown();
                    }
                }
            }
        }
    }
}
